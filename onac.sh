#!/bin/sh
# File: /usr/local/bin/onac.sh
# the hash-bang is vital for eudev to be able to execute this script.
date -u "+$( hostname -s ) %s %FT%TZ now using utility power" >> /var/log/battery.log
