#!/bin/sh
# File: /usr/local/bin/onbattery.sh
# the hash-bang is vital for eudev to be able to execute this script.
date -u "+$( hostname -s ) %s %FT%TZ now running on battery" >> /var/log/battery.log
